#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# Just type make to generate all necessary files
#
# Need to download YUI compressor at http://developer.yahoo.com/yui/compressor/
# and adapt the path to your system

yuic=java -jar ~/yuicompressor-2.4.7/build/yuicompressor-2.4.7.jar

#
# 
#
build=js/OpenLayers-2.10/build
oplib=js/OpenLayers-2.10/lib


htdocs=htdocs
htex=$(htdocs)/samples
htjs=$(htdocs)/js
css=$(htdocs)/css
plib=$(htdocs)/libs

samples = $(shell find samples/ -type f -name "sample*.html" -print | sed -e 's/samples/htdocs\/samples/' )

phplibs = $(shell find phplibs/ -type f -name "*.php" -print | sed -e 's/phplibs/htdocs\/libs/' )

version = $(shell mktemp -q -t cartosm-XXXXXXXX-cartosm >> /dev/shm/cartosm.build ; head -1 /dev/shm/cartosm.build | cut -d '-' -f 2 )

all: $(htdocs)/iframe.php $(htdocs)/bureaux.html $(htdocs)/test_bureaux.html $(htdocs)/references.html $(htdocs)/faq.html $(htdocs)/api.html $(htdocs)/mainkml.php $(phplibs) $(samples) $(htex)/bureaux.html $(htdocs)/test_iframe_full.html $(htdocs)/test_iframe_kml.html

tests:
	phpunit phplibs/TestUrlParser.php
	phpunit phplibs/TestKmlLoader.php

clean:
	rm -f $(css)/map-*.css bureaux.cfg $(build)/OpenLayersCartOSM.js $(build)/OpenLayersFrame.js $(htdocs)/iframe.php $(build)/iframe.cfg $(htdocs)/test_bureaux.html $(htdocs)/bureaux.html /dev/shm/cartosm.build $(htex)/sample*.html /tmp/cartosm-*-cartosm $(htdocs)/references.html $(htdocs)/faq.html $(htdocs)/api.html $(htdocs)/mainkml.php $(htjs)/addons-*.js $(htjs)/bureaux.js $(htjs)/OpenLayersCartOSM-*.js $(htjs)/OpenLayersFrame*.js $(htjs)/iframe-*.js $(css)/iframe-*.css $(phplibs)

$(htjs)/iframe-$(version).js: js/iframe.js js/common.js
	$(yuic) -o $@ --type js js/iframe.js
	$(yuic) --type js js/common.js >> $@

$(htjs)/addons-$(version).js: js/addons.js
	$(yuic) -o $@ --type js js/addons.js

$(css)/map-$(version).css: map.css
	$(yuic) -o $@ --type css map.css

$(css)/iframe-$(version).css: theme/default/style.css
	$(yuic) -o $@ --type css theme/default/style.css

$(htjs)/OpenLayersCartOSM-$(version).js: $(build)/OpenLayersCartOSM.js js/LICENSE
	cat js/LICENSE > $@
	cat $(build)/OpenLayersCartOSM.js >> $@

$(htjs)/OpenLayersFrame-$(version).js: $(build)/OpenLayersFrame.js js/LICENSE
	cat js/LICENSE > $@
	cat $(build)/OpenLayersFrame.js >> $@

$(htjs)/OpenLayersFrameKML-$(version).js: $(build)/OpenLayersFrameKML.js js/LICENSE
	cat js/LICENSE > $@
	cat $(build)/OpenLayersFrameKML.js >> $@

$(build)/OpenLayersCartOSM.js: $(build)/bureaux.cfg $(oplib)/bureaux.js
	cd $(build) ; python build.py bureaux OpenLayersCartOSM.js ; cd -

$(build)/OpenLayersFrame.js: $(build)/iframe.cfg
	cd $(build) ; python build.py iframe OpenLayersFrame.js ; cd -

$(build)/OpenLayersFrameKML.js: $(build)/iframekml.cfg
	cd $(build) ; python build.py iframekml OpenLayersFrameKML.js ; cd -

$(htdocs)/iframe.php: iframe.php $(htjs)/OpenLayersFrameKML-$(version).js $(htjs)/OpenLayersFrame-$(version).js $(css)/iframe-$(version).css $(htjs)/iframe-$(version).js
	sed -e 's/OpenLayersFrame-[0-9a-zA-Z]*.js/OpenLayersFrame-$(version).js/' iframe.php > $@
	sed -i -e 's/OpenLayersFrameKML-[0-9a-zA-Z]*.js/OpenLayersFrameKML-$(version).js/' $@
	sed -i -e 's/iframe-[0-9a-zA-Z]*.\([js|css]\)/iframe-$(version).\1/' $@
	sed -i -e 's/^\/\/.*//' $@
	sed -i -e '/^$$/d' $@

$(htdocs)/bureaux.html: bureaux.html addons.html body-close.html footer.html $(htjs)/OpenLayersCartOSM-$(version).js $(css)/map-$(version).css $(htdocs)/theme/default/style.pack.css $(htjs)/addons-$(version).js
	sed -e 's/OpenLayersCartOSM-[0-9a-zA-Z]*.js/OpenLayersCartOSM-$(version).js/' bureaux.html > $@
	sed -i -e 's/map-[0-9a-zA-Z]*.css/map-$(version).css/' $@
	cat footer.html >> $@
	cat addons.html >> $@
	sed -i -e 's/addons-[0-9a-zA-Z]*.js/addons-$(version).js/' $@
	cat body-close.html >> $@
	sed -i -e 's/<!-- .*-->//' $@

$(htdocs)/mainkml.php: $(htdocs)/bureaux.html
	cp $(htdocs)/bureaux.html $@
	sed -i -e 's/OpenLayersCartOSM-[0-9a-zA-Z]*.js/OpenLayers-2.10.js/'  $@
	sed -i -e 's/map-[0-9a-zA-Z]*.css/map-$(version).css/' $@

$(htdocs)/references.html: references.html footer.html $(css)/map-$(version).css $(htdocs)/theme/default/style.pack.css
	cp references.html $@
	cat footer.html >> $@
	cat body-close.html >> $@
	sed -i -e 's/map-[0-9a-zA-Z]*.css/map-$(version).css/' $@

$(htdocs)/faq.html: faq.html footer.html $(css)/map-$(version).css $(htdocs)/theme/default/style.pack.css
	cp faq.html $@
	cat footer.html >> $@
	cat body-close.html >> $@
	sed -i -e 's/OpenLayersFrame-[0-9a-zA-Z]*.js/OpenLayersFrame-$(version).js/g' $@
	sed -i -e 's/OpenLayersFrameKML-[0-9a-zA-Z]*.js/OpenLayersFrameKML-$(version).js/g' $@
	sed -i -e 's/map-[0-9a-zA-Z]*.css/map-$(version).css/' $@

$(htdocs)/api.html: api.html footer.html $(css)/map-$(version).css $(htdocs)/theme/default/style.pack.css
	cp api.html $@
	cat footer.html >> $@
	cat body-close.html >> $@
	sed -i -e 's/map-[0-9a-zA-Z]*.css/map-$(version).css/' $@

$(htdocs)/test_bureaux.html: test_bureaux.html $(htjs)/tests/bureaux.js $(htjs)/tests/common.js $(htjs)/tests/nominatim.js $(htjs)/OpenLayersCartOSM-$(version).js $(htjs)/tests/addons.js
	cp test_bureaux.html $@
	sed -i -e 's/OpenLayersCartOSM-[0-9a-zA-Z]*.js/OpenLayersCartOSM-$(version).js/' $@
	sed -i -e 's/addons-[0-9a-zA-Z]*.js/addons-$(version).js/' $@

$(htdocs)/test_iframe.html: test_iframe.html $(htjs)/tests/iframe.js $(htjs)/tests/common.js
	cp test_iframe.html $@
	sed -i -e 's/OpenLayersFrame-[0-9a-zA-Z]*.js/OpenLayersFrame-$(version).js/' $@
	sed -i -e 's/OpenLayersFrameKML-[0-9a-zA-Z]*.js/OpenLayersFrameKML-$(version).js/' $@

$(htdocs)/test_iframe_full.html: test_iframe_full.html $(htjs)/tests/iframe.js
	cp test_iframe_full.html $@
	sed -i -e 's/iframe-[0-9a-zA-Z]*.js/iframe-$(version).js/' $@

$(htdocs)/test_iframe_kml.html: test_iframe_kml.html $(htjs)/tests/iframe.js $(htjs)/OpenLayersFrameKML-$(version).js
	cp test_iframe_kml.html $@
	sed -i -e 's/OpenLayersFrameKML-[0-9a-zA-Z]*.js/OpenLayersFrameKML-$(version).js/' $@

$(build)/iframe.cfg: iframe.cfg $(oplib)/iframe.js $(oplib)/common.js
	cp iframe.cfg $@

$(build)/iframekml.cfg: iframekml.cfg $(oplib)/iframe.js $(oplib)/common.js
	cp iframekml.cfg $@

$(build)/bureaux.cfg: bureaux.cfg
	cp bureaux.cfg $@

bureaux.cfg: iframe.cfg
	sed -e 's/iframe.js/bureaux.js/' iframe.cfg > $@

$(oplib)/bureaux.js: js/bureaux.js js/nominatim.js
	cp js/bureaux.js $@
	cat js/nominatim.js >> $@
	cp js/bureaux.js $(htjs)/bureaux.js

$(oplib)/common.js: js/common.js
	cp js/common.js $@
	cp js/common.js $(htjs)/common.js

$(oplib)/iframe.js: js/iframe.js
	cp js/iframe.js $@

$(htjs)/tests/:
	mkdir -p $(htjs)/tests/

$(plib):
	mkdir -p $(plib)

$(htjs)/tests/bureaux.js: js/tests/bureaux.js $(htjs)/tests/
	cp js/tests/bureaux.js $@

$(htjs)/tests/common.js: js/tests/common.js $(htjs)/tests/
	cp js/tests/common.js $@

$(htjs)/tests/addons.js: js/tests/addons.js $(htjs)/tests/
	cp js/tests/addons.js $@

$(htjs)/tests/iframe.js: js/tests/iframe.js $(htjs)/tests/
	cp js/tests/iframe.js $@

$(htjs)/tests/nominatim.js: js/tests/nominatim.js $(htjs)/tests/
	cp js/tests/nominatim.js $@

$(htdocs)/theme/default/style.css: theme/default/style.css
	mkdir -p $(htdocs)/theme/default/
	cp theme/default/style.css $@

$(htdocs)/theme/default/style.pack.css: $(htdocs)/theme/default/style.css
	$(yuic) -o $@ --type css $(htdocs)/theme/default/style.css

$(htex)/bureaux.html: $(htex)/sample1.html
	cat $(htex)/sample1.html > $@

$(samples): samples/header.html footer.html $(css)/map-$(version).css samples/list.html
	cat samples/header.html > $@
	name=`echo $@ | sed -e 's/htdocs\/samples/samples/' ` ; cat $$name >> $@
	cat footer.html >> $@
	cat body-close.html >> $@
	sed -i -e 's/map-[0-9a-zA-Z]*.css/map-$(version).css/' $@
	sed -i -f samples/list.html $@

$(phplibs): $(plib)
	name=`echo $@ | sed -e 's/htdocs\/libs/phplibs/' ` ; cat $$name > $@
	sed -i -e 's/OpenLayers\(.*\)-[0-9a-zA-Z]*.js/OpenLayers\1-$(version).js/' $@
	sed -i -e 's/^\/\/.*//' $@