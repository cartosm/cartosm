/**
 *
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// The map object
var map;

function init(lon, lat, zoom, isMarker, isNav, isPan, icon, zoomCtrl, kml) {

    OpenLayers.ImgPath = "http://cartosm.eu/js/img/";

    // Create a new object map
    map = new OpenLayers.Map('map', {controls: [] });
    
    // Create the base layer (common.js)
    var layerOSM = createLayer();

    // Create center
    var center = new OpenLayers.LonLat(lon,lat).transform(new OpenLayers.Projection("EPSG:4326"),  new OpenLayers.Projection("EPSG:900913") );

    map.addLayer(layerOSM);

    if ( isMarker ) {
	var layerMarkers = new OpenLayers.Layer.Markers("Markers");
	// Create icon object
	iconA = getCartOsmIcon(icon);

	// Création du point central de la carte
	var myMarker = new OpenLayers.Marker(center, iconA);
	layerMarkers.addMarker(myMarker);

	map.addLayer(layerMarkers);
    }

    if ( isNav ) {
	map.addControl( new OpenLayers.Control.Navigation() );
    }

    if ( isPan ) {

	if ( zoomCtrl == 'bar' ) {
	    map.addControl( new OpenLayers.Control.PanZoomBar() );	    
	} else {
	    map.addControl( new OpenLayers.Control.PanZoom() );
	}

    }

    var ctrlAttr = new OpenLayers.Control.Attribution();
    map.addControl( ctrlAttr );

    // Positionnement de la carte sur le point central
    map.setCenter(center, zoom);

    // Layer for KML datas
    if ( kml ) {
	addKML( kml );
    }
}
