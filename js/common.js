/**
 *
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var iconA;
var iconName = 'down';
var zoomCtrl = 'inout';

var selectedFeature = null;

/**
 * Create the base layer
 *
 */
function createLayer () {

    var servers = ['http://a.tile.cartosm.eu/${z}/${x}/${y}.png', 
		   'http://b.tile.cartosm.eu/${z}/${x}/${y}.png', 
		   'http://c.tile.cartosm.eu/${z}/${x}/${y}.png'];

    attrib = 'Data CC-By-SA by <a target="_new" href="http://openstreetmap.org/">OpenStreetMap</a>, Service <a target="_new" href="http://cartosm.eu">Cartosm</a>';
    
    // Create a new layer object
    var layer = new OpenLayers.Layer.OSM("OSM", 
					 servers, 
					 { attribution: attrib }
					);

    return layer;
}

/**
 * Add a KML layer
 *
 */

function addKML(urlkml) {

    if (urlkml.length > 15) {

	var kmlStyle = defDefaultStyle();
	var kmlStyleMap = defDefaultStyleMap(kmlStyle);
	
	var kmlFormat = new OpenLayers.Format.KML({ extractStyles: true, 
						    extractAttributes: true,
						    maxDepth: 2
						  })
	

	var kmlProto = new OpenLayers.Protocol.HTTP({ url: urlkml, format: kmlFormat });
	
	var kmlLayer = new OpenLayers.Layer.Vector("KML", 					       
						   { projection: "EPSG:4326",
						     strategies: [new OpenLayers.Strategy.Fixed()],
						     protocol: kmlProto,
						     styleMap: kmlStyleMap
						     
						   });
	
	
	map.addLayer(kmlLayer);
	
	// Gestion des popups
	// global var used on onPopupClose
	selectControl = new OpenLayers.Control.SelectFeature(kmlLayer,
							     { onSelect: onFeatureSelect, 
							       onUnselect: onFeatureUnselect
							     }
							    );
	
	map.addControl(selectControl);
	selectControl.activate();
    }
    //map.addControl(new OpenLayers.Control.LayerSwitcher());
    
}

function onPopupClose(evt) {
    if (selectedFeature) {
	selectControl.unselect(selectedFeature);
    }
}
 
function onFeatureSelect(feature) {
    // used on onPopupClose
    selectedFeature = feature;

    var text = feature.attributes.description;

    var popup = new OpenLayers.Popup("csmKmlPopup", 
                                     feature.geometry.getBounds().getCenterLonLat(),
                                     new OpenLayers.Size(420,210),
                                     text,
                                     true, 
				     onPopupClose);



    feature.popup = popup;
    popup.setOpacity(0.8);
    map.addPopup(popup);
}
 
function onFeatureUnselect(feature) {
    map.removePopup(feature.popup);
    feature.popup.destroy();
    feature.popup = null;
}

function getCartOsmIconUrl (name) {

    var secname = name.substring(0,24);
    var url = 'http://cartosm.eu/images/mapping/' + secname + '.png';

    return url;
}

function getCartOsmIcon (name) {

    var secname = name.substring(0,24);
    var size = getCartOsmIconSize(name);
    var offset = null;
    var sizeO = new OpenLayers.Size(size[0],size[1]);


    switch (secname) {
    case 'down':
	offset = new OpenLayers.Pixel(- 16, -37);
	break;
    case 'up':
	offset = new OpenLayers.Pixel(- 16, -2);
	break;
    case 'left':
	offset = new OpenLayers.Pixel(0, -20);
	break;
    case 'right':
	offset = new OpenLayers.Pixel(-37, -17);
	break;
    default:
	offset = new OpenLayers.Pixel(- 16, -37);
    }
    
    var url = getCartOsmIconUrl(secname);

    var icon = new OpenLayers.Icon(url, sizeO ,offset);

    return icon;
}

function getCartOsmIconSize (name) {

    var size = new Array();

    switch (name) {
    case 'down':
	size = Array(32,37);
	break;
    case 'up':
	size = Array(32,37);
	break;
    case 'left':
	size = Array(37,32);
	break;
    case 'right':
	size = Array(37,32);
	break;
    }

    return size;
}

function defDefaultStyle () {
    var styl = new OpenLayers.Style({ pointRadius: 8,
				      fillColor: "blue",
				      fillOpacity: 0.8,
				      strokeColor: "#5555ff",
				      strokeWidth: 2,
				      strokeOpacity: 0.8
				    });
    
    return styl;
}

function defDefaultStyleMap (style) {

    var StyleMap = new OpenLayers.StyleMap({ "default": style,
					     "select": {
						 fillColor: "#8aeeef",
						 strokeColor: "#32a8a9"
					     }
					   });
    
    
    return StyleMap;
}
