/**
 *
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//
// Globals vars
//
var map;
//
// Define initial states
//
optdivStatus = true;
taidivStatus = true;
kmldivStatus = false;
//
function init(){
    // Création d'un objet map
    map = new OpenLayers.Map('map', {controls: [] });
    
    OpenLayers.ImgPath = "http://cartosm.eu/js/img/";

    var layerOSM = createLayer();

    var markers = new OpenLayers.Layer.Markers("Markers");

    layerOSM.events.register("moveend", layerOSM, genIfrmCode);

    document.getElementById("inputMark").checked = true;
    document.getElementById("movemap").checked = true;
    document.getElementById("inputArrow").checked = true;

    var llz = getCenterArray();
    
    iconA = getCartOsmIcon(llz[3]);

    // Création du point central de la carte
    var center = new OpenLayers.LonLat(llz[0],llz[1]).transform(new OpenLayers.Projection("EPSG:4326"),  new OpenLayers.Projection("EPSG:900913") );

    if ( document.getElementById("inputMark").checked ) {
	// Création du point central de la carte
	var myMarker = new OpenLayers.Marker(center, iconA);
	markers.addMarker(myMarker);
    }
    
    // Ajout du layer à la carte
    map.addLayers([layerOSM, markers] );

    // Positionnement de la carte sur le point central
    map.setCenter(center, llz[2]);

    map.addControl( new OpenLayers.Control.Attribution() );

    movemapOpt(true);
    arrowsOpt(true);

    document.getElementById("address").value = llz[4];

    initialPana = document.getElementById("pana").innerHTML;

    //
    copyMsg = "Coller ici le code de votre iframe actuelle";
    document.getElementById("actual").value = copyMsg;

    //
    document.getElementById("descone").innerHTML = $.gt.gettext("descone");
}
/**
 * Return Navigator language, for futur i18n
 *
 */
function readLang() {
    if (navigator) {
	return navigator.language;
    } else {
	return "en";
    }
}

/**
 * Set the map center
 *
 *
 */
function mapSetCenter(lon, lat, zoom) {

    if ( zoom > 0 ) {    
	// Création du point central de la carte
	var center = new OpenLayers.LonLat(lon,lat).transform(new OpenLayers.Projection("EPSG:4326"),  new OpenLayers.Projection("EPSG:900913") );

	// Positionnement de la carte sur le point central
	map.setCenter(center, zoom);
    }
}


function readMarker () {
    try { value = document.getElementById("inputMark").checked; }
    catch(err) { value = false; }

    return value;
}


//
// Display a link under the frame
//

function osmlinkOpt(status) {

    if ( status == false ) {
    
	document.getElementById("output").style.height = "180px";

    } else {

	document.getElementById("output").style.height = "260px";

    }

    genIfrmCode();
}

function showIcons(status) {

    iderr = "hiddenIcons";
    
    
    if ( document.getElementById(iderr).style.display == 'none' )
    {
    	document.getElementById(iderr).style.display = 'inline-block';

    } else {

    	document.getElementById(iderr).style.display = 'none';

    }

}

//
// Can move map
//

function movemapOpt(status) {

    var ctrls = map.getControlsByClass("OpenLayers.Control.Navigation");

    if (ctrls.length > 0 ) {
	ctrls[0].destroy();
    } else {
	map.addControl( new OpenLayers.Control.Navigation() );
    }

    genIfrmCode();
}

//
// Show PanZoom on map
//
function arrowsOpt(status) {

    changeArrows();

    document.getElementById("inputZoomBar").disable = status;

    genIfrmCode();

    return 0;
}

function changeArrows () {
    var ctrls = '';

    if ( zoomCtrl == 'inout' ) {
	ctrls = map.getControlsByClass("OpenLayers.Control.PanZoom");
    } else {
	ctrls = map.getControlsByClass("OpenLayers.Control.PanZoomBar");
    }

    if (ctrls.length > 0 ) {
	ctrls[0].destroy();
    } else {
	if ( zoomCtrl == 'inout' ) {
	    map.addControl( new OpenLayers.Control.PanZoom() );
	} else {
	    map.addControl( new OpenLayers.Control.PanZoomBar() );
	}
    }

    return 0;
}

//
// Show PanZoom on map
//
function zoombarOpt(status) {

    if ( zoomCtrl == 'inout' ) {
	ctrls = map.getControlsByClass("OpenLayers.Control.PanZoom");
    } else {
	ctrls = map.getControlsByClass("OpenLayers.Control.PanZoomBar");
    }

    ctrls[0].destroy();

    if ( status == true ) {
	zoomCtrl = 'bar';
    } else {
	zoomCtrl = 'inout';
    }

    arrowsOpt();
}

//
// Show central marker on map
//
function markerdis(status) {

    if ( status ) {
	
	var lm = map.getLayersByName("Markers")[0];
	myMarker = new OpenLayers.Marker(map.getCenter(), iconA);
	
	lm.addMarker(myMarker);

	document.getElementById("icons").style.display = "block";
	document.getElementById("moreIcons").style.display = "block";

	
    } else {
	var lm = map.getLayersByName("Markers")[0];
	
	lm.removeMarker(lm.markers[0]);

	document.getElementById("icons").style.display = "none";
	document.getElementById("moreIcons").style.display = "none";
    }
    
    genIfrmCode();
}
/**
 * Determines whether a value is an Integer
 *
 */
function isInteger(x)
{
    var y=parseInt(x);
    if (isNaN(y)) return false;
    return x==y && x.toString()==y.toString(); 
}
/**
 * Return a random number with a max value defined by range
 *
 */
function getRandom(range) {

    var r_no = Math.floor(Math.random() * range);

    return r_no;

}
/**
 * Define some places
 *
 *
 */
function getPreDefCenters() {

    var pos = new Array();

    pos[0] = [-1.54674,47.21932,17,'down',"10, rue Georges Clémenceau Nantes"]; // Musée de Nantes
    pos[1] = [5.57602,50.64095,16,'university',"université de Liège"];
    pos[2] = [-7.97405,12.665,17,'down',"boulevard nelson mandela bamako"];
    pos[3] = [3.01568,50.61682,18,'school',"école louise michel, loos"];

    pos[4] = [-0.57183,44.84063,17,'down',"8 place du parlement, bordeaux"]; // La Machine à Lire Bordeaux
    pos[5] = [1.44041,43.60449,18,'right',"77, rue Pargaminières Toulouse"]; // Tire Lire Toulouse
    pos[6] = [2.37044,48.87844,17,'down',"place du colonnel fabien paris"]; // place du colonnel fabien
    pos[7] = [-3.34078,47.72617,18,'library',"médiathèque locmiquélic"];
    pos[8] = [-4.36164,48.37108,18,'left',"salle Avel Vor Plougastel-Daoulas"];
    pos[9] = [2.33642,48.86088,16,'museum',"musée du Louvre Paris"];
    pos[10] = [3.87360,43.60826,18,'left',"13 bd du jeu de paume montpellier"]; // Le grain des Mots Montpellier
    pos[11] = [7.74468,48.58130,18,'down',"120 Grand'Rue Strasbourg"]; // Quai des Brumes Strasbourg
    pos[12] = [3.05447,50.62951,18,'down',"154 rue léon gambetta lille"]; // Le bateau Livre Lille

    return pos;
}
/**
 * Return a random place
 *
 *
 */
function getCenterArray() {

    var pos = getPreDefCenters();

    var r_no = getRandom( pos.length );

    return pos[r_no];
}
/**
 * Show central marker on map
 *
 */
function changeIcon(name) {

    var status = readMarker();

    if ( status == true ) {

	chgIcon(name);
	
	genIfrmCode();
    }
}

function chgIcon(name) {

    iconName = name;

    iconA = getCartOsmIcon (name);
    
    var lm = map.getLayersByName("Markers")[0];
    
    lm.removeMarker(lm.markers[0]);
    
    myMarker = new OpenLayers.Marker(map.getCenter(), iconA);
	
    lm.addMarker(myMarker);
	
    return 0;
}



function optdiv(img) {

    var display = true;

    if ( optdivStatus ) {
	optdivStatus = false;
	img.src = "images/divopen.png";
	display = "none";
    } else {
	optdivStatus = true;
	img.src = "images/divclose.png";	    
	display = "block";
    }

    document.getElementById("inputIcons").style.display = display;
    document.getElementById("divInputMovemap").style.display = display;
    document.getElementById("divInputArrow").style.display = display;
    document.getElementById("divInputZoomBar").style.display = display;
    document.getElementById("divOsmLink").style.display = display;

    document.getElementById("icons").style.display = display;
    document.getElementById("moreIcons").style.display = display;


}

function taillediv(img) {

    var display = true;

    if ( taidivStatus ) {
	taidivStatus = false;
	img.src = "images/divopen.png";
	display = "none";
    } else {
	taidivStatus = true;
	img.src = "images/divclose.png";	    
	display = "block";
    }

    document.getElementById("divInputTaille").style.display = display;
}

function kmldiv(img) {

    var display = true;

    if ( kmldivStatus ) {
	taidivStatus = false;
	img.src = "images/divopen.png";
	display = "none";
    } else {
	kmldivStatus = true;
	img.src = "images/divclose.png";	    
	display = "block";
    }

    document.getElementById("divInputKML").style.display = display;
}
