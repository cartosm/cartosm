//
// Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

//
// Additionnal functions
//
// This script is loaded at the end of the document, be aware.
//

//
// Generate iframe code
//
function genIfrmCode () {

    var z = map.getZoom();
    
    var width = readWidth();
    var height = readHeight();

    var isMarker = readMarker();

    var center = map.getCenter().transform(map.getProjectionObject(), new OpenLayers.Projection("EPSG:4326") );

    var begin = '&lt;iframe width="'+width+'" height="'+height+'" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="';
    var end = '"&gt;&lt;/iframe&gt;';

    var host = begin + "http://cartosm.eu/map";

    var code = host + "?lon=" + center.lon + "&lat=" + center.lat + "&zoom=" + z ;
	
    code = code + "&width=" + width;
    code = code + "&height=" + height;
    code = code + "&mark=" + isMarker;
    code = code + "&nav=" + document.getElementById("movemap").checked;
    code = code + "&pan=" + document.getElementById("inputArrow").checked;
    code = code + "&zb=" + zoomCtrl;
    code = code + "&style=default";
    code = code + "&icon=" + iconName;
    code = code + end;


    if (document.getElementById("osmlinkInput").checked ) {
	code = code + '&lt;br /&gt;&lt;div id="cartosmlink"&gt;&lt;a href="http://www.openstreetmap.org/?';
	if ( isMarker == true ) {
	    code = code + 'mlat=' + center.lat + '&mlon=' + center.lon + '&zoom=' + z + '&layers=M" style="text-align:left;"&gt;';
	} else {
	    code = code + 'lat=' + center.lat + '&lon=' + center.lon + '&zoom=' + z + '&layers=M" style="text-align:left;"&gt;';
	}
	code = code + 'Voir sur une carte plus grande&lt;/a&gt;';
	code = code + '&lt;/div&gt;';
    }

    document.getElementById("output").innerHTML = code;

    if ( isMarker == true ) {

	var lm = map.getLayersByName("Markers")[0];

	lm.removeMarker(lm.markers[0]);

	// Création du point central de la carte
	myMarker = new OpenLayers.Marker(map.getCenter(), iconA);
	
	lm.addMarker(myMarker);
    }
}
/**
 * Read and return the iframe width from html document input value
 *
 */
function readWidth () {
    try { width = document.getElementById("inputWidth").value; }
    catch(err) { width = 350; }

    return width;
}

/**
 * Read and return the iframe height from html document input value
 *
 */

function readHeight () {
    try { width = document.getElementById("inputHeight").value; }
    catch(err) { width = 400; }

    return width;
}
/**
 *
 *
 *
 */

function inpCheckLarg(id, iderr) {

    var value = document.getElementById(id).value;

    if (isPositiveInteger(value) == false || value > 3000 || value < 20 ) {
	document.getElementById(iderr).innerHTML = 'Saisie invalide !';
	document.getElementById(iderr).style.display = 'inline-block';

        document.getElementById(id).style.background = 'pink'; 
        document.getElementById(id).style.border = '1px solid #ff0000';

	return false;

    } else {
	document.getElementById(iderr).style.display = 'none';

        document.getElementById(id).style.background = 'none'; 
        document.getElementById(id).style.border = '1px solid #ddd';

	return true;
    }
}

function inpLarg(id, iderr) {

    if (inpCheckLarg(id, iderr) ) { genIfrmCode(); }
}

/**
 *
 *
 *
 */

function closeMoreIcons() {

    var div = document.getElementById("moreIcons");
    div.style.backgroundColor = "#ffffff";
    div.style.border = "none";
    div.innerHTML = '<a href="#map" onclick="showMoreIcons();">Plus d\'icônes</a>';
    div.style.marginBottom = "6px";
}
/**
 *
 *
 *
 */

function showMoreIcons(){
    // Création d'un objet map
    var div = document.getElementById("moreIcons");
    var html = '<!-- -->';

    div.style.border = "1px solid #181818";

    div.style.marginBottom = "12px";
    div.style.padding = "2px";
    div.style.backgroundColor = "#dcdcdc";

    var imgs = ArrayIcons();

    for (var i = 0 ; i < imgs.length ; i++ ) {
	
	name = imgs[i].replace('\.png','');

	html += '<img alt="'+name+'" title="'+name+'" src="images/mapping/' + imgs[i] + '" ';
	html += 'onclick="changeIcon(' + "'" + name + "');" + '" />';

    }
    html += '<div style="text-align: right;color: black;"><a href="#map" onclick="closeMoreIcons();">Fermer</a></div>';
    div.innerHTML = html;
    
}
/**
 *
 *
 *
 */

function ArrayIcons() {
    var imgs = new Array();

    imgs.push('university.png');
    imgs.push('school.png');
    imgs.push('playground.png');
    imgs.push('library.png');
    imgs.push('nanny.png');
    imgs.push('nursery.png');

    imgs.push('museum.png');
    imgs.push('music-classical.png');
    imgs.push('music-hiphop.png');
    imgs.push('music-live.png');
    imgs.push('zoo.png');

    imgs.push('court.png');
    imgs.push('administration.png');
    imgs.push('bank.png');
    imgs.push('pizza.png');

    imgs.push('agriculture.png');
    imgs.push('airplane-tourism.png');

    imgs.push('bookstore.png');
    imgs.push('bread.png');
    imgs.push('butcher.png');
    imgs.push('toys.png');
    imgs.push('shoes.png');
    imgs.push('clothes.png');
    imgs.push('perfumery.png');
    imgs.push('convenience.png');

    imgs.push('camping.png');
    imgs.push('cemetary.png');
    imgs.push('chapel.png');
    imgs.push('church.png');

    imgs.push('disability.png');
    imgs.push('fire.png');

    imgs.push('doctor.png');
    imgs.push('firstaid.png');
    imgs.push('hairsalon.png');
    imgs.push('hospital.png');
    imgs.push('ophthalmologist.png');
    imgs.push('wifi.png');
    imgs.push('yoga.png');

    return imgs;
}
/**
 * Clear the textarea where we paste the iframe's code
 *
 */
function clearActual () {

    code = document.getElementById("actual").value;

    if (code == copyMsg) {
	document.getElementById("actual").value = '';
    }

    document.getElementById("butAnalyser").style.display = "block";

    return document.getElementById("actual").value;
}
/**
 *
 *
 *
 */

function lookAndCenter () {
    if (_lookAndCenter() )
 	genIfrmCode();
}
/**
 *
 *
 *
 */

function _lookAndCenter () {

    code = document.getElementById("actual").value;
    
    if (code != copyMsg) {
	
	var datas = lookInCode(code);
	
	if ( checkValid(datas) ) {
	    
	    var lon    = datas[0];
	    var lat    = datas[1];
	    var zoom   = datas[2];
	    var width  = datas[3];
	    var height = datas[4];
	    
	    mapSetCenter(lon, lat, zoom);
	    
	    document.getElementById("pana").innerHTML = initialPana + "<br />Zoom : "+zoom+"<br />Latitude : "+lat+"<br />Longitude : "+lon;
	    
	    document.getElementById("inputHeight").value = height;
	    
	    document.getElementById("inputWidth").value = width;
	    
	    return true;
	}

    }
    return false;
}
/**
 *
 *
 *
 */
    
function lookInCode (code) {
    
    var datas = new Array();
    
    datas = grepGeorge(code);
    
    if (datas[0] < 0) {
	datas = grepBob(code);
    }

    var lon    = datas[0];
    var lat    = datas[1];
    var zoom   = datas[2];
    var width  = datas[3];
    var height = datas[4];

    return datas;
}
/**
 * Check an array and return if it's contains all valid values
 * for create an iframe
 *
 */

function checkValid( datas ) {

    var valid = true;

    valid = valid && isFinite( datas[0] );
    valid = valid && isFinite( datas[1] );
    valid = valid && isPositiveInteger( datas[2] );
    valid = valid && isPositiveInteger( datas[3] );
    valid = valid && isPositiveInteger( datas[4] );

    return valid;
}
/**
 * Check if a value in an integer and is positive
 *
 *
 */

function isPositiveInteger (value) {
    if ( isInteger(value) && value >= 0) {
	return true;
    } else {
	return false;
    }
    
}
/**
 * Check if a value is positive
 *
 *
 */

function isPositiveFinite (value) {
    if ( isFinite(value) && value >= 0) {
	return true;
    } else {
	return false;
    }
    
}

function grepValue (code, reg) {
    var value = -1;    
    var re = new RegExp(reg);

    if (re.test(code)) { 
	var vals = re.exec(code);

	if (vals != null) {
	    if (vals.length > 2) {
		value = vals[vals.length - 1]; 
	    } else {
		value = vals[1]; 
	    }
	} else {
	    value = -3;
	}
    } else {
	value = -2;
    }
    
    return value;
}

function grepGeorge (code) {

    var zoom   = grepValue(code, /z=(\d\d?)/)
    var lat    = grepValue(code, /ll=(-?\d\d?\.?\d+),/)
    var lon    = grepValue(code, /ll=-?\d\d?\.?\d+,(-?\d\d?\.?\d+)/)
    var width  = grepValue(code, /width=["'](\d\d+)["']/i);
    var height = grepValue(code, /height=["']?(\d\d+)["']?/i)

    return [lon, lat, zoom, width, height];
}

function grepBob (code) {

    var zoom   = grepValue(code, /lvl=(\d\d?)/)
    var lat    = grepValue(code, /cp=(-?\d\d?\.?\d+)~/)
    var lon    = grepValue(code, /cp=-?\d\d?\.?\d+~(-?\d\d?\.?\d+)/)
    var width  = grepValue(code, /width=["'](\d\d+)["']/i);
    var height = grepValue(code, /height=["']?(\d\d+)["']?/i)

    return [lon, lat, zoom, width, height];
}
