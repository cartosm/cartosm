//
// Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
module("iframe");


test("init", function(){

    var mapdiv = document.createElement('div');
    mapdiv.id = "map";
    mapdiv.style.width = 200;
    mapdiv.style.height = 120;
    document.body.appendChild(mapdiv);

    init(-2,42,7,false,false,false,'','inout');

    var center =  map.getCenter().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326") );

    equals(map.getZoom(), 7);
    equals(center.lon, -2);
    equals(center.lat, 42);
    equals(map.getNumLayers(), 1);
});
