//
// Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
module("nominatim", {

    teardown: function() {

	try { document.body.removeChild(document.getElementById('nominresult')) ; }
	catch (err) { err+= 1 }

	try { document.body.removeChild(document.getElementById('nominsearch')) ; }
	catch (err) { err+= 1 }

	try { document.body.removeChild(document.getElementById('nominnb')) ; }
	catch (err) { err+= 1 }

    }
});

test("doBasicClick", function(){
    var res = document.createElement('div');
    res.id = "nominresult";
    document.body.appendChild(res);

    var res = document.createElement('span');
    res.id = "nominnb";
    document.body.appendChild(res);

    var ser = document.createElement('div');
    ser.id = "nominsearch";
    document.body.appendChild(ser);

    map = new OpenLayers.Map('map', {controls: [] });
    var layerOSM = createLayer();
    map.addLayer(layerOSM );

    var before = document.getElementsByTagName("script").length;

    mapSetCenter(42,42,6);

    // do the action
    doBasicClick("10 rue georges clemenceau nantes");

    // read results
    var after = document.getElementsByTagName("script").length;


    // Do the tests
    equals(after, before + 1);
});

test("hideResults", function(){

    var res = document.createElement('div');
    res.id = "nominresult";
    document.body.appendChild(res);

    var ser = document.createElement('div');
    ser.id = "nominsearch";
    document.body.appendChild(ser);

    hideResults();

    equals(document.getElementById("nominresult").innerHTML, '');
    equals(document.getElementById("nominresult").style.display, "none");
    equals(document.getElementById("nominsearch").style.marginBottom, "8px");

});

test("buildURL", function(){

    var res = buildURL("10 rue georges clemenceau nantes");

    equals(res, "http://open.mapquestapi.com/nominatim/v1/search?format=json&json_callback=parseJres&q=10 rue georges clemenceau nantes" );

});

test("parseJres", function(){
    /*
     *
     */
    var res = document.createElement('span');
    res.id = "nominnb";
    document.body.appendChild(res);

    var dres = document.createElement('div');
    dres.id = "nominresult";
    document.body.appendChild(dres);

    var ser = document.createElement('div');
    ser.id = "nominsearch";
    document.body.appendChild(ser);

    map = new OpenLayers.Map('map', {controls: [] });

    var layerOSM = createLayer();

    map.addLayer(layerOSM );

    mapSetCenter(6,6,6);

    parseJres([{"place_id":"78919021","licence":"Data Copyright OpenStreetMap Contributors, Some Rights Reserved. CC-BY-SA 2.0.","osm_type":"way","osm_id":"89085459","boundingbox":["47.1706695556641","47.1729850769043","-1.57722735404968","-1.57515645027161"],"lat":"47.1718195564248","lon":"-1.5762002945404","display_name":"Rue Georges Clemenceau, Ouche Blanche, Pôle de proximité Sud-Ouest, Bouguenais, Nantes Métropole, Loire-Atlantique, Pays de la Loire, 44400, France","class":"highway","type":"residential"},{"place_id":"73919711","licence":"Data Copyright OpenStreetMap Contributors, Some Rights Reserved. CC-BY-SA 2.0.","osm_type":"way","osm_id":"82898977","boundingbox":["47.175178527832","47.177978515625","-1.57970988750458","-1.57830452919006"],"lat":"47.1765678250345","lon":"-1.57903735321434","display_name":"Rue Georges Clemenceau, Les Couëts, Pôle de proximité Sud-Ouest, Bouguenais, Nantes Métropole, Loire-Atlantique, Pays de la Loire, 44400, France","class":"highway","type":"residential"}]);

    equals(document.getElementById("nominnb").innerText, "2 réponses trouvées");

    var newcent= map.getCenter().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326") );

    equals(newcent.lat , 47.171819556425 );
    equals(newcent.lon ,-1.5762002945404 );

});

test("getLonLatFromNominResult", function(){
    /*
     *
     */
    var res = [{"place_id":"78919021","licence":"Data Copyright OpenStreetMap Contributors, Some Rights Reserved. CC-BY-SA 2.0.","osm_type":"way","osm_id":"89085459","boundingbox":["47.1706695556641","47.1729850769","-1.5772273968","-1.57515645027161"],"lat":"47.17148342","lon":"-1.56207094542","display_name":"Rue Georges Clemenceau, Ouche Blanche, Pôle de proximité Sud-Ouest, Bouguenais, Nantes Métropole, Loire-Atlantique, Pays de la Loire, 44400, France","class":"highway","type":"residential"},{"place_id":"73919711","licence":"Data Copyright OpenStreetMap Contributors, Some Rights Reserved. CC-BY-SA 2.0.","osm_type":"way","osm_id":"82898977","boundingbox":["47.175178527832","47.177978515625","-1.57970988750458","-1.57830452919006"],"lat":"47.1765678250345","lon":"-1.57903735321434","display_name":"Rue Georges Clemenceau, Les Couëts, Pôle de proximité Sud-Ouest, Bouguenais, Nantes Métropole, Loire-Atlantique, Pays de la Loire, 44400, France","class":"highway","type":"residential"}];

    equals(getLonLatFromNominResult(res)[0] , -1.56207094542 );
    equals(getLonLatFromNominResult(res)[1] , 47.17148342 );

    equals(getLonLatFromNominResult('foobar')[0] , null );
    equals(getLonLatFromNominResult('foo')[1] , null );

});