//
// Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
module("addons");

test("ArrayIcons", function(){
    var icons = ArrayIcons();
    ok (icons.length, 38);
});

test("grepValue", function(){
    equals(grepValue("foo?z=12&foo=bar", /z=(\d\d?)/), 12);
    equals(grepValue("foo?z=12&foo=bar", /foo=(\w+)/), "bar");
});

test("grepGeorge", function(){

    var code ='<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.fr/?ie=UTF8&amp;ll=46.75984,1.738281&amp;spn=7.933115,19.577637&amp;z=6&amp;output=embed"></iframe><br /><small><a href="http://maps.google.fr/?ie=UTF8&amp;ll=46.75984,1.738281&amp;spn=7.933115,19.577637&amp;z=6&amp;source=embed" style="color:#0000FF;text-align:left">Agrandir le plan</a></small>';

    var vals = grepGeorge(code);

    equals( vals[0] , 1.738281);
    equals( vals[1] , 46.75984);
    equals( vals[2] , 6);
    equals( vals[3] , 425);
    equals( vals[4] , 350);
});

test("grepBob", function(){
    var code = '<div id="mapviewer"><iframe id="map" scrolling="no" width="500" height="400" frameborder="0" src="http://www.bing.com/maps/embed/?v=2&amp;cp=48.107776641~-1.6802769889999913&amp;lvl=5&amp;dir=0&amp;sty=r&amp;emid=527fb6eb-64ec-8bfb-d2e1-5d3f25cc7546"></iframe><div id="LME_maplinks" style="line-height:20px;"><a id="LME_largerMap" href="http://www.bing.com/maps/?v=2&amp;cp=48.107776641~-1.6802769889999913&amp;lvl=5&amp;dir=0&amp;sty=r" target="_blank" style="margin:0 7px">Afficher une carte plus grande</a><a id="LME_directions" href="http://www.bing.com/maps/?v=2&amp;cp=48.107776641~-1.6802769889999913&amp;lvl=5&amp;dir=0&amp;sty=r&amp;rtp=%7Epos.48.107776641_-1.6802769889999913_" target="_blank" style="margin:0 7px">Itinéraire</a></div></div>';

    var vals = grepBob(code);

    equals( vals[0] , -1.6802769889999913);
    equals( vals[1] , 48.107776641);
    equals( vals[2] , 5);
    equals( vals[3] , 500);
    equals( vals[4] , 400);
});

test("readWidth", function(){
    // readWidth return a default value in case of error
    equals( readWidth(), 350);

    // create the right input element
    var elmt = document.createElement('input');
    elmt.id = "inputWidth";
    elmt.value = "200";
    document.body.appendChild(elmt);

    equals( readWidth(), 200);

    document.body.removeChild(elmt);
});

test("readHeight", function(){
    equals( readHeight(), 400);

    // create the right input element
    var elmt = document.createElement('input');
    elmt.id = "inputHeight";
    elmt.value = "200";
    document.body.appendChild(elmt);

    equals( readHeight(), 200);

    document.body.removeChild(document.getElementById('inputHeight'));
});


test("showMoreIcons ", function(){						   

    var elmt = document.createElement('div');
    elmt.id = "moreIcons";
    document.body.appendChild(elmt);

    var items = document.getElementsByTagName("img");
    var before = items.length;

    showMoreIcons();

    items = document.getElementsByTagName("img");
    var after = items.length;

    ok(after > before);

    // clean document
    document.body.removeChild(elmt);
});

test("closeMoreIcons ", function(){						   

    var elmt = document.createElement('div');
    elmt.id = "moreIcons";
    elmt.innerHTML = '<img src="foo.png" />';
    document.body.appendChild(elmt);

    var before = document.getElementsByTagName("img").length;
    
    equals(before, 1);

    closeMoreIcons();

    var after = document.getElementsByTagName("img").length;

    ok(after < before);

    // clean document
    document.body.removeChild(elmt);
});

test("clearActual", function(){

    copyMsg = "foo";

    // create the right input element
    var elmt = document.createElement("textarea");
    elmt.id = "actual";
    elmt.value = copyMsg;
    document.body.appendChild(elmt);

    var but = document.createElement("anchor");
    but.id = "butAnalyser";
    document.body.appendChild(but);

    equals(clearActual(), '');

    elmt.value = "bar";

    equals(clearActual(), "bar");

    // clean document
    document.body.removeChild(elmt);
    document.body.removeChild(but);
});

test("lookInCode", function(){
    var code ='<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.fr/?ie=UTF8&amp;ll=46.75984,1.738281&amp;spn=7.933115,19.577637&amp;z=6&amp;output=embed"></iframe><br /><small><a href="http://maps.google.fr/?ie=UTF8&amp;ll=46.75984,1.738281&amp;spn=7.933115,19.577637&amp;z=6&amp;source=embed" style="color:#0000FF;text-align:left">Agrandir le plan</a></small>';

    var result = lookInCode(code);

    equals(result[0], 1.738281);
    equals(result[1], 46.75984);
    equals(result[2], 6);
    equals(result[3], 425);
    equals(result[4], 350);
});

test("checkValid", function(){
    equals(checkValid( [ 42,  42,  16,  420, 340] ), true);
    equals(checkValid( [-42, -42,  16,  420, 340] ), true);
    equals(checkValid( [-42.4, -2.4,  16,  420, 340] ), true);
    equals(checkValid( [ 42,  42, -16,  420, 340] ), false);
    equals(checkValid( [ 42,  42,  16, -420,   0] ), false);

});

test("isPositiveInteger", function(){
    equals(isPositiveInteger( 0 ), true);
    equals(isPositiveInteger( 1 ), true);
    equals(isPositiveInteger(-10 ), false);
    equals(isPositiveInteger( 10.9 ), false);
    equals(isPositiveInteger( -1.2 ), false);

});

test("isPositiveFinite", function(){
    equals(isPositiveFinite( 0 ), true);
    equals(isPositiveFinite( 1 ), true);
    equals(isPositiveFinite( 1.32 ), true);
    equals(isPositiveFinite( -10 ), false);
    equals(isPositiveFinite( -1.2 ), false);
    equals(isPositiveFinite( 'foo' ), false);
    equals(isPositiveFinite( '2e3' ), true);
});


test("_lookAndCenter", function(){

    var elmth = document.createElement('input');
    elmth.id = "inputHeight";
    elmth.value = "200";
    document.body.appendChild(elmth);

    var elmtw = document.createElement('input');
    elmtw.id = "inputWidth";
    elmtw.value = "200";
    document.body.appendChild(elmtw);

    var pana = document.createElement("div");
    pana.id = "pana";
    document.body.appendChild(pana);

    map = new OpenLayers.Map('map', {controls: [] });

    var layerOSM = createLayer();

    map.addLayer(layerOSM );

    mapSetCenter(42,42,6);
    copyMsg = "foo";
    initialPana = "foo";

    var code ='<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.fr/?ie=UTF8&amp;ll=46.784,1.7381&amp;spn=7.933115,19.577637&amp;z=6&amp;output=embed"></iframe><br /><small><a href="http://maps.google.fr/?ie=UTF8&amp;ll=46.75984,1.738281&amp;spn=7.933115,19.577637&amp;z=6&amp;source=embed" style="color:#0000FF;text-align:left">Agrandir le plan</a></small>';

    // create the right input element
    var elmt = document.createElement("textarea");
    elmt.id = "actual";
    elmt.value = code;
    document.body.appendChild(elmt);

    _lookAndCenter();

    //
    var newcent= map.getCenter().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326") );

    equals(newcent.lat , 46.784);
    equals(newcent.lon , 1.7381);

    // clean document
    document.body.removeChild(elmt);
    document.body.removeChild(elmtw);
    document.body.removeChild(elmth);
    document.body.removeChild(pana);
});

test("inpCheckLarg", function(){

    // create the right input element
    var elmt = document.createElement('input');
    elmt.id = "foo";
    elmt.value = "200";
    document.body.appendChild(elmt);

    var bar = document.createElement('div');
    bar.id = "bar";
    document.body.appendChild(bar);

    equals( inpCheckLarg("foo","bar"), true);

    elmt.value = "13450";
    equals( inpCheckLarg("foo","bar"), false);

    document.body.removeChild(elmt);
    document.body.removeChild(bar);
});
