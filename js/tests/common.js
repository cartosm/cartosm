//
// Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
module("common");

test("getCartOsmIconSize ", function(){						   
    equals(32, getCartOsmIconSize('down')[0] );
    equals(37, getCartOsmIconSize('down')[1] );
    equals(37, getCartOsmIconSize('left')[0] );
    equals(32, getCartOsmIconSize('left')[1] );
});

test("getCartOsmIcon ", function(){
    val = getCartOsmIcon('down');
    equals('object', typeof val );
});

test("getCartOsmIconUrl ", function(){

    var url = getCartOsmIconUrl('down');
    equals(url, 'http://cartosm.eu/images/mapping/down.png' );

    url = getCartOsmIconUrl('agriculture');
    equals(url, 'http://cartosm.eu/images/mapping/agriculture.png' );

    url = getCartOsmIconUrl('wheniwillbeadultiwillcutwoodinthebiggestforestoftheworld');
    equals(url, 'http://cartosm.eu/images/mapping/wheniwillbeadultiwillcut.png' );
});

// Check if we are all necessary libs
test("test zoomin ", function(){						   
    var obj = new OpenLayers.Control.ZoomIn();
    equals('object', typeof obj );

});
// Check if we are all necessary libs
test("test zoomOut ", function(){						   
    var obj = new OpenLayers.Control.ZoomOut();
    equals('object', typeof obj );

});
// Check if we are all necessary libs
test("test panzoombar ", function(){						   
    var obj = new OpenLayers.Control.PanZoomBar();
    equals('object', typeof obj );

});

test("createLayer ", function(){						   
    var obj = createLayer();
    equals('object', typeof obj );

    name = obj.name;
    equals('OSM', name);

    equals((obj.attribution.length > 32), true);

});

test("addKML", function(){
    map = new OpenLayers.Map('map', {controls: [] });
    center = new OpenLayers.LonLat(0,1);

    equals(map.getNumLayers(), 0);

    addKML('http://carto.quiedeville.org/cartosm/dechet.kml');

    equals(map.getNumLayers(), 1);
});

test("defDefaultStyle", function(){
    
    var st = defDefaultStyle();

    equals(typeof(st), "object");
});

test("defDefaultStyleMap", function(){
    
    var st = defDefaultStyleMap();

    equals(typeof(st), "object");
});
