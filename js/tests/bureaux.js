//
// Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
module("bureaux", { 

    teardown: function() {
	try { document.body.removeChild(document.getElementById('inputHeight')); }
	catch (err) { err+= 1 }
	try { document.body.removeChild(document.getElementById('inputWidth')); }
	catch (err) { err+= 1 }

	try { document.body.removeChild(document.getElementById('actual')) ; }
	catch (err) { err+= 1 }

	try { document.body.removeChild(document.getElementById('nominresult')) ; }
	catch (err) { err+= 1 }

	try { document.body.removeChild(document.getElementById('nominsearch')) ; }
	catch (err) { err+= 1 }

	try { document.body.removeChild(document.getElementById('butAnalyser')); }
	catch (err) { err+= 1 }
    }
});

test("isInteger", function(){
    equals(isInteger("1"), true );
    equals(isInteger(100), true );
    equals(isInteger(-100), true );
    equals(isInteger("1h"), false );
    equals(isInteger(100.7), false );
    equals(isInteger(-100.7), false );
});

test("getRandom ", function(){
    for (var i = 1 ; i < 12 ; i += 1) {
	pos = getRandom(i);	
	ok (pos >= 0 );
	ok (pos < i );
    }
});

test("getPreDefCenters ", function(){
    var arrs = getPreDefCenters();
    ok (arrs.length > 10 );

    equals(arrs[2].length, 5);
});


test("getCenterArray ", function(){
    pos = getCenterArray();	
    ok (pos[2] > 0 );
    ok (pos[2] < 19 );    
});

test("readLang", function(){
    var lang = readLang();
    ok ((lang == "en" || lang == "fr") , true );
});

test("readMarker", function(){
    ok(readMarker() == false);

    // create the right input element
    var elmt = document.createElement('input');
    elmt.id = "inputMark";
    elmt.checked = true;
    document.body.appendChild(elmt);

    equals( readMarker(), true);

    // clean document
    document.body.removeChild(elmt);
});

test("chgIcon", function(){
    map = new OpenLayers.Map('map', {controls: [] });
    lm = new OpenLayers.Layer.Markers("Markers");
    iconA = getCartOsmIcon('down');
    center = new OpenLayers.LonLat(0,1);
    myMarker = new OpenLayers.Marker(center, iconA);
    lm.addMarker(myMarker);
    map.addLayer(lm);
    var result = chgIcon('up');
    ok(result == 0);
    ok(iconName == 'up');
    chgIcon('down');
    ok(result == 0);
    ok(iconName == 'down');
});

test("changeArrows", function(){
    map = new OpenLayers.Map('map', {controls: [] });
    zoomCtrl='bar';
    var result = changeArrows();
    ok(result == 0);

    zoomCtrl='inout';
    var result = changeArrows();
    ok(result == 0);
});




test("mapSetCenter", function(){
    /*
     *
     */
    map = new OpenLayers.Map('map', {controls: [] });

    var layerOSM = createLayer();

    map.addLayer(layerOSM );

    mapSetCenter(42,42,6);

    var newcent= map.getCenter().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326") );

    equals(newcent.lat , 42 );
    equals(newcent.lon , 42 );
    /*
     * With a negative zoom set setcenter is not executed
     */
    mapSetCenter(46,6,-6);

    newcent= map.getCenter().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326") );

    equals(newcent.lat , 42 );
    equals(newcent.lon , 42 );

});

/*
function osmlinkOpt(status) {
function showIcons(status) {
function movemapOpt(status) {
function arrowsOpt(status) {
function changeArrows () {
function zoombarOpt(status) {
function markerdis(status) {

function optdiv(img) {
function taillediv(img) {
function kmldiv(img) {
*/