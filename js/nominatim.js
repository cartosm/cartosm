/**
 *
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function buildURL(optquery) {
    var host_url = 'http://open.mapquestapi.com';
    var full_url = host_url + '/nominatim/v1/search?format=json&json_callback=parseJres';

    return full_url + "&q=" + optquery;
};

function doBasicClick(query) {
    searchType = 'basic';
    var script = document.createElement('script');
    script.type = 'text/javascript';   
    script.src = buildURL(query);
    document.body.appendChild(script);
};

function buildNominResult(datas, zoom) {
    var html = '<ul>';

    for(var i =0; i < datas.length; i++){

	var result = datas[i];
	
	html += '<li><a href="#" onclick="mapSetCenter(' + result.lon + ',' + result.lat + ',' + zoom + ');">';
	html += result.display_name + '</a>';	
    }
    
    html += '</ul>';
    html += '<p class="closer"><a href="#" onclick="hideResults();">fermer</a></p>';
    return html;
}

function getLonLatFromNominResult(datas) {

    var result = Array();

    try {
	result[1] = datas[0].lat;
	result[0] = datas[0].lon;
    }
    catch (err) {
	append(result, null);
	append(result, null);
    }

    return result;
}

function parseJres(response) {
    // Parse results datas
    var zoom = 16;
    var lat = null;
    var lon = null;

    if (response) {
	sphtml = response.length + " réponses trouvées";
	document.getElementById("nominnb").innerText = sphtml;

	html = buildNominResult(response, zoom);
	
	lon = getLonLatFromNominResult(response)[0];
	lat = getLonLatFromNominResult(response)[1];

	if ( (lon != null) && (lat != null) ) { 
	    // Positionnement de la carte sur le point central
	    var center = new OpenLayers.LonLat(lon,lat).transform(new OpenLayers.Projection("EPSG:4326"),  new OpenLayers.Projection("EPSG:900913") );
	    map.setCenter(center, zoom);

	    document.getElementById("nominresult").innerHTML = html;
	    document.getElementById("nominresult").style.display = "block";
	    document.getElementById("nominsearch").style.marginBottom = "1px";
	}
	else
	{
	    document.getElementById("nominresult").innerHTML = "Aucun résultat, essayez une autre recherche...";
	}

    } 
    else 
    {
	document.getElementById("nominresult").innerHTML = "Error querying Nominatim !";

    }

}

function hideResults() {
    // Hide resuls div

    document.getElementById("nominresult").innerHTML='';
    document.getElementById("nominresult").style.display = "none";
    document.getElementById("nominsearch").style.marginBottom = "8px";
}
