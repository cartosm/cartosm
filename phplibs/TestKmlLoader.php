<?php
/**
 *
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'PHPUnit/Framework.php';

require_once("KmlLoader.php");

class TestKmlLoader extends PHPUnit_Framework_TestCase
{

  public function test_url_to_file()
  {
    $kl = new KmlLoader();
    $stack = $kl->url_to_file("foo/bar/");

    $this->assertEquals($stack, "foo_bar_");
 
  }

  public function test_needUpdate()
  {
    $kl = new KmlLoader();
    $filename = "/dev/shm/foobar";
    @unlink($filename);

    $foo = $kl->needUpdate($filename);
    $this->assertEquals($foo, True);

    touch($filename);

    $foo = $kl->needUpdate($filename);

    $this->assertEquals($foo, False);

  }

}
?>
