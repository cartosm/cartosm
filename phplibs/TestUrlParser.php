<?php
/**
 *
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'PHPUnit/Framework.php';


require_once("UrlParser.php");

class TestUrlParser extends PHPUnit_Framework_TestCase
{

  public function testMinimal()
  {

    $parms = array ('lon' => 42.2 ,
		    'lat' => 42,
		    'zoom' => 17 );

    $foo = new UrlParser($parms);

    $this->assertEquals($foo->lon, 42.2);
    $this->assertEquals($foo->lat, 42);
    $this->assertEquals($foo->zoom, 17);
    $this->assertEquals($foo->width, 350);
    $this->assertEquals($foo->height, 350);
    $this->assertEquals($foo->marker, 'false'); 
  }

  public function testMoreComplete()
  {

    $parms = array ('lon' => 2.2 ,
		    'lat' => 4.2,
		    'zoom' => 16,
		    'mark' => 'TRUE',
		    'icon' => 'farm',
		    'width' => 420,
		    'height' => 440
		    );

    $foo = new UrlParser($parms);

    $this->assertEquals($foo->lon, 2.2);
    $this->assertEquals($foo->lat, 4.2);
    $this->assertEquals($foo->zoom, 16);
    $this->assertEquals($foo->width, 420);
    $this->assertEquals($foo->height, 440);
    $this->assertEquals($foo->marker, 'true');
    $this->assertEquals($foo->icon, 'farm'); 
  }


  public function testBadValues()
  {

    $parms = array ('lon' => 2.2 ,
		    'lat' => 4.2,
		    'zoom' => 16,
		    'mark' => 'foobar',
		    'icon' => 'farmer12345678901234XXXXXXXXXXXXXXXX',
		    'width' => -420,
		    'height' => -440,
		    'zb' => 'foo'
		    );

    $foo = new UrlParser($parms);

    $this->assertEquals($foo->lon, 2.2);
    $this->assertEquals($foo->lat, 4.2);
    $this->assertEquals($foo->zoom, 16);
    $this->assertEquals($foo->width, 350);
    $this->assertEquals($foo->height, 350);
    $this->assertEquals($foo->marker, 'false');
    $this->assertEquals($foo->icon, 'farmer12345678901234');
    $this->assertEquals($foo->zb, 'inout');
 
  }

  public function testKML()
  {

    $parms = array ('lon' => 42.2 ,
		    'lat' => 42,
		    'zoom' => 17,
		    'kml' => 'http%3A%2F%2Ffoo.bar%2Fbar');

    $foo = new UrlParser($parms);

    $this->assertEquals($foo->lon, 42.2);
    $this->assertEquals($foo->lat, 42);
    $this->assertEquals($foo->zoom, 17);
    $this->assertEquals($foo->width, 350);
    $this->assertEquals($foo->kml, 'http://foo.bar/bar');
    $this->assertEquals($foo->libjs, null);
  }

  public function testKMLFull()
  {

    $parms = array ('lon' => 42.2 ,
		    'lat' => 42,
		    'zoom' => 17,
		    'kml' => 'http%3A%2F%2Ffoo.bar%2Fbar',
		    'libjs' => "full");

    $foo = new UrlParser($parms);

    $this->assertEquals($foo->lon, 42.2);
    $this->assertEquals($foo->lat, 42);
    $this->assertEquals($foo->zoom, 17);
    $this->assertEquals($foo->width, 350);
    $this->assertEquals($foo->kml, 'http://foo.bar/bar');
    $this->assertEquals($foo->libjs, 'OpenLayers-2.10.js');
  }



}
?>
