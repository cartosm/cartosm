<?php
/**
 *
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class KmlLoader
{
  // define properties


  // constructor
  public function __construct() {

    $this->datadir = "datas/";

  }


  public function url_to_file($url) {

    $filename = str_replace("/","_", $url);

    return $filename;
  }

  /**
   * Is a file need to be download
   *
   * Args : (string) Filename to test
   * Return : boolean
   *  - true is the file need to be download, False if not
   */
  public function needUpdate($filename) {
    
    $result = True;
    
    $now = time();
    
    if (file_exists($filename)) {
      
      $ftime = filectime($filename);
      
      if ($now < ($ftime + (15 * 60 ) ) ) {
	$result = False;
      }
      
    }

    return $result;
  }

  /**
   * Download a file and store it on the filesystem
   *
   * Args : (string) url
   * Return : boolean
   *
   */
  
  public function fetch_kml($url) {
    $result = 0;
    $urc = $this->url_to_file($url);
    
    $ch = curl_init($url);
    
    $filename = $this->datadir . $urc;
    
    if ($this->needUpdate($filename)) {
      
      $fp = fopen($filename, "w");
      
      if ( $fp === FALSE ) {
	
	print $filename;
	
      } else {
	curl_setopt($ch, CURLOPT_FILE, $fp);

	// Perhaps will use the remote filetime in future
	//curl_setopt($ch, CURLOPT_FILETIME, True);

	curl_setopt($ch, CURLOPT_HEADER, 0);

	// Do the job
	curl_exec($ch);
	
	$infos = curl_getinfo($ch);
	
	curl_close($ch);
	fclose($fp);
	
	return $infos['http_code'];
	
      }
    }
  }

}

?>
