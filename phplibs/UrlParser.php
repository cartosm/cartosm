<?php
/**
 *
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class UrlParser
{
  // define properties
  public $lon;
  public $lat;
  public $zoom;
  public $marker;

  // constructor
  public function __construct($parms) {
    $this->error = 0;
    $this->error_str = array();
    $this->width = 350;
    $this->height = 350;
    $this->lon = 0;
    $this->lat = 0;
    $this->zoom = 12;
    $this->marker = 'false';
    $this->nav = 'false';
    $this->pan = 'false';
    $this->icon = 'down';
    $this->kml = null;
    $this->libjs = null;
    
    $this->libfull = "OpenLayers-2.10.js";
    // This values will be changed by Makefile
    $this->libmin = "OpenLayersFrame-foobar42.js";
    $this->libkml = "OpenLayersFrameKML-foobar42.js";
    // End of variable part

    $this->_lonlatParse($parms);
    $this->_zoomParse($parms);
    $this->_iconParse($parms);
    $this->_controlParse($parms);
    $this->_sizeParse($parms);
    $this->_kmlParse($parms);

    //
    if ( count( $this->error_str ) > 0 ) {
      $this->error = 1 ;
    }
  }

  private function _kmlParse($parms) {

    if (array_key_exists('kml', $parms)) {
      $this->kml = urldecode($parms['kml']);
    }

    if (array_key_exists('libjs', $parms)) {
      $this->libjs = ($parms['libjs'] == 'full') ? $this->libfull : $parms['libjs'];
    }

  }

  private function _sizeParse($parms) {

    if (array_key_exists('width', $parms)) 
      {
	$width = substr($parms['width'], 0, 5);
	$this->width = ($width > 0) ? $width : 350;
      }

    if (array_key_exists('height', $parms)) 
      {
	$height = substr($parms['height'], 0, 5);
	$this->height = ($height > 0) ? $height : 350;
      }

  }


  private function _controlParse($parms) {

    if (array_key_exists('nav', $parms)) 
      {
	$this->nav = (strtolower($parms['nav']) == 'true') ? 'true' : 'false';
      }

    if (array_key_exists('pan', $parms)) 
      {
	$this->pan = (strtolower($parms['pan']) == 'true') ? 'true' : 'false';
      }

    if (array_key_exists('zb', $parms)) 
      {
	$this->zb = (strtolower($parms['zb']) == 'bar') ? 'bar' : 'inout';
      }
  }


  private function _iconParse($parms) {

    if (array_key_exists('mark', $parms)) 
      {
	$this->marker = (strtolower($parms['mark']) == 'true') ? 'true' : 'false';
      }

    if (array_key_exists('icon', $parms)) 
      {
	$this->icon = substr($parms['icon'],0,20);
      }
  }


  private function _zoomParse($parms) {

    if (array_key_exists('zoom', $parms)) 
      {

	$zoom = substr($parms['zoom'],0 ,2 );

	$nbz = preg_match('/^\d\d?$/', $zoom, $matches);
	
	if ($nbz === 1) {
	  
	  $this->zoom = $matches[0];
	  
	} else {
	  $this->error = 1;
	  array_push($this->error_str, sprintf("Bad value '%s' for zoom", $zoom));
	}
	
      }	
    else 
      {
	array_push($this->error_str, "zoom is not defined");
      }
  }


  private function _lonlatParse($parms) {

    $pattern = '/^-?\d\d*\.?\d*$/';

    if (array_key_exists('lon', $parms)) {

      $lon = substr($parms['lon'], 0, 12);

      $nbz = preg_match($pattern, $lon, $matches);

      if ($nbz === 1) {

	$this->lon = $matches[0];

      } else {
	array_push($this->error_str, sprintf("Bad value '%s' for <b>lon</b>", $lon));
      }


    }

    if (array_key_exists('lat', $parms)) {

      $lat = substr($parms['lat'], 0, 12);

      $nbz = preg_match($pattern, $lat, $matches);

      if ($nbz === 1) {

	$this->lat = $matches[0];

      } else {
	array_push($this->error_str, sprintf("Bad value '%s' for <b>lat</b>", $lat));
      }

    }

  }


}
?>
