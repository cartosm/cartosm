<?PHP
/*
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include('libs/KmlLoader.php');
include('libs/UrlParser.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?PHP   
$error = 0;
$error_str = array();
//
// Parse the URL parameters
//
$u = new UrlParser($_GET);
//
// Default values
//
$libjs = "OpenLayersFrame-1297112553.js";
$parms = sprintf("%s,%s,%s,%s,%s,%s,'%s','%s'", $u->lon, $u->lat, $u->zoom, $u->marker, $u->nav, $u->pan, $u->icon, $u->zb);
//
// KML File url
//
$httpcode = 0;
$kml = '';

//
// Load an external KML File
//
if ( $u->kml != null ) {

  $kl = new KmlLoader();

  $kmlfile = "datas/" . $kl->url_to_file($u->kml);

  $httpcode = $kl->fetch_kml($u->kml);

  //
  //

  $libjs = "OpenLayersFrameKML-1297112553.js";
  $parms = sprintf("%s,%s,%s,%s,%s,%s,'%s','%s', '%s'", $u->lon, $u->lat, $u->zoom, $u->marker, $u->nav, $u->pan, $u->icon, $u->zb, $kmlfile);
} 
//
//
//
printf('<style type="text/css">#map { width: %spx; height: %spx; }', $u->width, $u->height);
print "\n.olControlAttribution{bottom: 3px!important;}</style>";
// foobar42 will be replace by Makefile
print '<link rel="stylesheet" href="css/iframe-foobar42.css" type="text/css" />';

if ( $u->libjs == null ) {
  printf('<script type="text/javascript" src="js/%s"></script>', $libjs);
} else {

  print '<script type="text/javascript" src="js/OpenLayers-2.10.js"></script>'."\n";
  print '<script type="text/javascript" src="js/iframe-foobar42.js"></script>';
}

print "\n</head>\n";

if ($u->error === 0) {

  printf('<body onload="init(%s)">',$parms);

  print '<div id="map"></div>';

} else {

  print '<body>';

  foreach ($u->error_str as $str) {
    $error_msg .= sprintf("<li>%s</li>", $str);
  }
  printf ('<div id="map" style="border: 2px solid red;"><h1>Error</h1><p>url parameters read : %s</p><p><ul>%s</ul></p></div>', $parms, $error_msg);
}
//
// Deal external errors
//
if ($httpcode == 404) {
  printf ('<div>Error 404 : <p>Unable to read %s</p></div>', $kml);
}

?>
  </body>
</html>
